package com.company;

import com.company.exception.NullObjectException;

public class Admin {
    private String adminName;
    private int identityNumber;

    public Admin(String adminName, int identityNumber) {
        if(adminName==null){
            throw new NullObjectException("admin should be entered");
        }

        this.adminName = adminName;
        this.identityNumber = identityNumber;
    }

    public String getAdminName() {
        return adminName;
    }

    public void setAdminName(String adminName) {
        this.adminName = adminName;
    }

    public int getIdentityNumber() {
        return identityNumber;
    }

    public void setIdentityNumber(int identityNumber) {
        this.identityNumber = identityNumber;
    }
}
