package com.company.exception;

public class InvalidBatchException extends RuntimeException {
    public InvalidBatchException(){
        super(" Entered batch is not correct");
    }
}
