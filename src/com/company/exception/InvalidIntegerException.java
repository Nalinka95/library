package com.company.exception;

public class InvalidIntegerException extends RuntimeException{
    public InvalidIntegerException(String msg){
        super(msg);
    }
}
