package com.company.exception;

public class NullObjectException extends RuntimeException {
    public NullObjectException(String msg){
        super(msg);
    }
}
