package com.company.exception;

public class InvalidRollNoException extends RuntimeException{
    public InvalidRollNoException(){
        super("Inserted Roll number is not in the valid range");
    }
}
