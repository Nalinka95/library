package com.company;

public class Main {

    public static void main(String[] args) {
	// write your code here
        Book[] myBooks=new Book[3];
        myBooks[0]=new Book("string theory",SubjectType.PHYSICS);
        myBooks[1]=new Book("probability",SubjectType.MATHS);
        myBooks[2]=new Book("Quantum",SubjectType.CHEMISTRY);

        Library schoolLibrary= new Library(myBooks);
        System.out.println("number of probability books : "+schoolLibrary.bookCount("probability"));
        schoolLibrary.addBook(new Book("Brain",SubjectType.BIO));
        System.out.println("..............");
        schoolLibrary.displayBooks();
        schoolLibrary.removeBook("probability");
        System.out.println("..........");
        schoolLibrary.displayBooks();

    }
}
