package com.company;

import com.company.exception.InvalidBatchException;
import com.company.exception.InvalidRollNoException;
import com.company.exception.NullObjectException;

public class Student {
    private int rollNumber;
    private String studentName;
    private int batch;
    private SubjectType subject;
    private Degree studentDegree;

    public Student(int rollNumber, String studentName, int batch, SubjectType subject, Degree studentDegree) {
        if(subject==null|| studentName==null|| studentDegree==null){
            throw new NullObjectException("subject, student name and student degree cannot be null");
        }
        else if(rollNumber<0){
            throw new InvalidRollNoException();
        }
        else if(!(batch>=14 && batch<21)){
            throw new InvalidBatchException();
        }
        else{
            this.rollNumber = rollNumber;
            this.studentName = studentName;
            this.batch = batch;
            this.subject = subject;
            this.studentDegree = studentDegree;
        }
    }
    public void getBook(String bookName){

    }

    public int getRollNumber() {
        return rollNumber;
    }

    public void setRollNumber(int rollNumber) {
        this.rollNumber = rollNumber;
    }

    public String getStudentName() {
        return studentName;
    }

    public void setStudentName(String studentName) {
        this.studentName = studentName;
    }

    public int getBatch() {
        return batch;
    }

    public void setBatch(int batch) {
        this.batch = batch;
    }

    public SubjectType getSubject() {
        return subject;
    }

    public void setSubject(SubjectType subject) {
        this.subject = subject;
    }

    public Degree getStudentDegree() {
        return studentDegree;
    }

    public void setStudentDegree(Degree studentDegree) {
        this.studentDegree = studentDegree;
    }
}
