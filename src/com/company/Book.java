package com.company;

import com.company.exception.NullObjectException;

public class Book {
    private String name;
    private SubjectType subject;

    public Book(String name, SubjectType subject) {
        if(name==null|| subject==null){
            throw new NullObjectException("Book's name and subject cannot be null");
        }
        this.name = name;
        this.subject = subject;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public SubjectType getSubject() {
        return subject;
    }

    public void setSubject(SubjectType subject) {
        this.subject = subject;
    }
}
