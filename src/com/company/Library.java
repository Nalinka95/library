package com.company;

import com.company.exception.NullObjectException;

public class Library {
    private  Book[] books;

    public Library(Book[] books) {
        if(books==null){
            throw new NullObjectException("books object cannot be null");
        }
        int nullCount=0;
        for(Book book: books){
            if(book==null){
                nullCount++;
            }
        }
        if(nullCount>0){
            throw new NullObjectException("all the books should not be null");
        }
        this.books=books;
    }
    public int bookCount(String bookName){
        if(bookName==null){
            throw new NullObjectException("book name cannot be null");
        }
        int bookCount=0;
        for(Book book: books){
            if(book.getName().equalsIgnoreCase(bookName)){
                bookCount++;
            }
        }
        return bookCount;
    }
    public void addBook(Book newBook){
        if(newBook==null){
            throw new NullObjectException("new book cannot be null");
        }
        int noOfUpdatedBooks=books.length+1;
        Book[] newBookArray=new Book[noOfUpdatedBooks];
        for(int index=0;index<books.length;index++){
            newBookArray[index]=books[index];
        }
        newBookArray[books.length]= newBook;
        setBooks(newBookArray);

    }
    public void removeBook(String removeBook){
        if(removeBook==null){
            throw new NullObjectException("a book cannot be null");
        }
        int noOfUpdatedBooks=books.length-1;
        Book[] newBookArray=new Book[noOfUpdatedBooks];
        int newBookCount=0;
        boolean bookIsInArray=false;
        for(int index=0;index<books.length;index++){
            if(!(books[index].getName().equalsIgnoreCase(removeBook))){
                newBookArray[newBookCount]=books[index];
                newBookCount++;
            }
            else if(!bookIsInArray && books[index].getName().equalsIgnoreCase(removeBook)){
                bookIsInArray=true;
            }
            else {
                newBookArray[newBookCount]=books[index];
                newBookCount++;
            }

        }
        if(!bookIsInArray && books[books.length-1].getName().equalsIgnoreCase(removeBook)){
            setBooks(newBookArray);
        }else if(bookIsInArray){
            newBookArray[newBookArray.length-1]=books[books.length-1];
            setBooks(newBookArray);
        }
        else if(!books[books.length-1].getName().equalsIgnoreCase(removeBook)){
            setBooks(books);
        }

    }


    public Book[] getBooks() {
        return books;
    }

    public void setBooks(Book[] books) {
        this.books = books;
    }
    public void displayBooks(){
        for(Book book: books){
            System.out.println("book name : "+book.getName());
            System.out.println("book category : "+book.getSubject()+"\n");
        }
    }
}
